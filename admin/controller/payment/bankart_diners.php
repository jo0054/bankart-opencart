	<?php 
class ControllerPaymentBankartDiners extends Controller {
	private $error = array(); 

	public function index() {
		$this->load->language('payment/bankart_diners');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('setting/setting');
			
			$this->model_setting_setting->editSetting('bankart_diners', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_off'] = $this->language->get('text_off');
		
		$this->data['entry_language'] = $this->language->get('entry_language');
		$this->data['entry_terminal_alias'] = $this->language->get('entry_terminal_alias');
		$this->data['entry_terminal_password'] = $this->language->get('entry_terminal_password');
		$this->data['entry_tranportal_id'] = $this->language->get('entry_tranportal_id');
		$this->data['entry_currency'] = $this->language->get('entry_currency');
		$this->data['entry_minimum_instalment'] = $this->language->get('entry_minimum_instalment');
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');
		$this->data['entry_declined_order_status'] = $this->language->get('entry_declined_order_status');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['minimum_instalment'])) {
			$this->data['error_minimum_instalment'] = $this->error['minimum_instalment'];
		} else {
			$this->data['error_minimum_instalment'] = '';
		}
		
 		if (isset($this->error['terminal_alias'])) {
			$this->data['error_terminal_alias'] = $this->error['terminal_alias'];
		} else {
			$this->data['error_terminal_alias'] = '';
		}
		
		if (isset($this->error['terminal_password'])) {
			$this->data['error_terminal_password'] = $this->error['terminal_password'];
		} else {
			$this->data['error_terminal_password'] = '';
		}

		if (isset($this->error['tranportal_id'])) {
			$this->data['error_tranportal_id'] = $this->error['tranportal_id'];
		} else {
			$this->data['error_tranportal_id'] = '';
		}

  		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->document->breadcrumbs[] = array(
       		'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_payment'),
      		'separator' => ' :: '
   		);

   		$this->document->breadcrumbs[] = array(
       		'href'      => $this->url->link('payment/bankart_diners', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
				
		$this->data['action'] = $this->url->link('payment/bankart_diners', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['languages'] =  array (
			'SI' => 'SI - Slovenscina',
			'BS' => 'BS - Bosanscina',
			'CZ' => 'CZ - Cescina',
			'DE' => 'DE - Nemscina',
			'ESP' => 'ESP - Spanscina',
			'HR' => 'HR - Hrvascina',
			'HU' => 'HU - Madzarscina',
			'IT' => 'IT - Italijanscina',
			'RUS' => 'RUS - Ruscina',
			'SVK' => 'SVK - Slovascina',
			'SR' => 'SR - Srbscina',
			'US' => 'US - Anglescina'
		);
		
		if (isset($this->request->post['bankart_diners_language'])) {
			$this->data['bankart_diners_language'] = $this->request->post['bankart_diners_language'];
		} else {
			$this->data['bankart_diners_language'] = $this->config->get('bankart_diners_language');
		}
		
		if (isset($this->request->post['bankart_diners_terminal_alias'])) {
			$this->data['bankart_diners_terminal_alias'] = $this->request->post['bankart_diners_terminal_alias'];
		} else {
			$this->data['bankart_diners_terminal_alias'] = $this->config->get('bankart_diners_terminal_alias');
		}
		
		if (isset($this->request->post['bankart_diners_terminal_password'])) {
			$this->data['bankart_diners_terminal_password'] = $this->request->post['bankart_diners_terminal_password'];
		} else {
			$this->data['bankart_diners_terminal_password'] = $this->config->get('bankart_diners_terminal_password');
		}

		if (isset($this->request->post['bankart_diners_minimum_instalment'])) {
			$this->data['bankart_diners_minimum_instalment'] = $this->request->post['bankart_diners_minimum_instalment'];
		} else {
			$this->data['bankart_diners_minimum_instalment'] = $this->config->get('bankart_diners_minimum_instalment');
		}
		
		if (isset($this->request->post['bankart_diners_tranportal_id'])) {
			$this->data['bankart_diners_tranportal_id'] = $this->request->post['bankart_diners_tranportal_id'];
		} else {
			$this->data['bankart_diners_tranportal_id'] = $this->config->get('bankart_diners_tranportal_id');
		}
		
		$this->load->model('localisation/currency');
		
		$this->data['currencies'] = $this->model_localisation_currency->getCurrencies();
		
		if (isset($this->request->post['bankart_diners_currency_id'])) {
			$this->data['bankart_diners_currency_id'] = $this->request->post['bankart_diners_currency_id'];
		} else {
			$this->data['bankart_diners_currency_id'] = $this->config->get('bankart_diners_currency_id'); 
		}
		
		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['bankart_diners_order_status_id'])) {
			$this->data['bankart_diners_order_status_id'] = $this->request->post['bankart_diners_order_status_id'];
		} else {
			$this->data['bankart_diners_order_status_id'] = $this->config->get('bankart_diners_order_status_id'); 
		}
		
		if (isset($this->request->post['bankart_diners_declined_order_status_id'])) {
			$this->data['bankart_diners_declined_order_status_id'] = $this->request->post['bankart_diners_declined_order_status_id'];
		} else {
			$this->data['bankart_diners_declined_order_status_id'] = $this->config->get('bankart_diners_declined_order_status_id'); 
		}
		
		if (isset($this->request->post['bankart_diners_geo_zone_id'])) {
			$this->data['bankart_diners_geo_zone_id'] = $this->request->post['bankart_diners_geo_zone_id'];
		} else {
			$this->data['bankart_diners_geo_zone_id'] = $this->config->get('bankart_diners_geo_zone_id'); 
		} 

		$this->load->model('localisation/geo_zone');
										
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['bankart_diners_status'])) {
			$this->data['bankart_diners_status'] = $this->request->post['bankart_diners_status'];
		} else {
			$this->data['bankart_diners_status'] = $this->config->get('bankart_diners_status');
		}
		
		if (isset($this->request->post['bankart_diners_sort_order'])) {
			$this->data['bankart_diners_sort_order'] = $this->request->post['bankart_diners_sort_order'];
		} else {
			$this->data['bankart_diners_sort_order'] = $this->config->get('bankart_diners_sort_order');
		}
		
		$this->template = 'payment/bankart_diners.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/bankart_diners')) {
			$this->error['terminal_alias'] = $this->language->get('error_terminal_alias');
		}
		
		if (!$this->request->post['bankart_diners_minimum_instalment']) {
			$this->error['minimum_instalment'] = $this->language->get('error_minimum_instalment');
		}
		
		if (!$this->request->post['bankart_diners_terminal_password']) {
			$this->error['terminal_password'] = $this->language->get('error_terminal_password');
		}

		if (!$this->request->post['bankart_diners_tranportal_id']) {
			$this->error['tranportal_id'] = $this->language->get('error_tranportal_id');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>