<?php echo $header; ?>
<div id="content">
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
	<div class="box">
	  <div class="left"></div>
	  <div class="right"></div>
	  <div class="heading">
		<h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
		<div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
	  </div>
	  <div class="content">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		  <table class="form">
		  	<tr>
			  <td><?php echo $entry_language; ?></td>
			  <td><select name="bankart_diners_language">
				  <?php foreach ($languages as $name => $value) { ?>
				  <?php if ($name == $bankart_diners_language) { ?>
				  <option value="<?php echo $name; ?>" selected="selected"><?php echo $value; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $name; ?>"><?php echo $value; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo $entry_terminal_alias; ?></td>
			  <td><input type="text" name="bankart_diners_terminal_alias" value="<?php echo $bankart_diners_terminal_alias; ?>" />
				<?php if ($error_terminal_alias) { ?>
				<span class="error"><?php echo $error_terminal_alias; ?></span>
				<?php } ?></td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo $entry_terminal_password; ?></td>
			  <td><input type="text" name="bankart_diners_terminal_password" value="<?php echo $bankart_diners_terminal_password; ?>" />
				<?php if ($error_terminal_password) { ?>
				<span class="error"><?php echo $error_terminal_password; ?></span>
				<?php } ?></td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo $entry_tranportal_id; ?></td>
			  <td><input type="text" name="bankart_diners_tranportal_id" value="<?php echo $bankart_diners_tranportal_id; ?>" />
				<?php if ($error_tranportal_id) { ?>
				<span class="error"><?php echo $error_tranportal_id; ?></span>
				<?php } ?></td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo $entry_minimum_instalment; ?></td>
			  <td><input type="text" name="bankart_diners_minimum_instalment" value="<?php echo $bankart_diners_minimum_instalment; ?>" />
				<?php if ($error_minimum_instalment) { ?>
				<span class="error"><?php echo $error_minimum_instalment; ?></span>
				<?php } ?></td>
			</tr>
			<tr>
			  <td><?php echo $entry_currency; ?></td>
			  <td><select name="bankart_diners_currency_id">
				  <?php foreach ($currencies as $currency) { ?>
				  <?php if ($currency['code'] == $bankart_diners_currency_id) { ?>
				  <option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['code']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $currency['code']; ?>"><?php echo $currency['code']; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			  <td><?php echo $entry_order_status; ?></td>
			  <td><select name="bankart_diners_order_status_id">
				  <?php foreach ($order_statuses as $order_status) { ?>
				  <?php if ($order_status['order_status_id'] == $bankart_diners_order_status_id) { ?>
				  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			  <td><?php echo $entry_declined_order_status; ?></td>
			  <td><select name="bankart_diners_declined_order_status_id">
				  <?php foreach ($order_statuses as $declined_order_status) { ?>
				  <?php if ($declined_order_status['order_status_id'] == $bankart_diners_declined_order_status_id) { ?>
				  <option value="<?php echo $declined_order_status['order_status_id']; ?>" selected="selected"><?php echo $declined_order_status['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $declined_order_status['order_status_id']; ?>"><?php echo $declined_order_status['name']; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			  <td><?php echo $entry_geo_zone; ?></td>
			  <td><select name="bankart_diners_geo_zone_id">
				  <option value="0"><?php echo $text_all_zones; ?></option>
				  <?php foreach ($geo_zones as $geo_zone) { ?>
				  <?php if ($geo_zone['geo_zone_id'] == $bankart_diners_geo_zone_id) { ?>
				  <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			  <td><?php echo $entry_status; ?></td>
			  <td><select name="bankart_diners_status">
				  <?php if ($bankart_diners_status) { ?>
				  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				  <option value="0"><?php echo $text_disabled; ?></option>
				  <?php } else { ?>
				  <option value="1"><?php echo $text_enabled; ?></option>
				  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				  <?php } ?>
				</select></td>
			</tr>
			<tr>
			  <td><?php echo $entry_sort_order; ?></td>
			  <td><input type="text" name="bankart_diners_sort_order" value="<?php echo $bankart_diners_sort_order; ?>" size="1" /></td>
			</tr>
		  </table>
		</form>
	  </div>
	</div>
</div>
<?php echo $footer; ?>