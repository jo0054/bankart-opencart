<?php
// Heading
$_['heading_title']      			= 'Bankart Diners';

// Text 
$_['text_payment']       			= 'Payment';
$_['text_success']       			= 'Success: You have modified Bankart Diners account details!';
$_['text_enabled']       			= 'Enabled';
$_['text_disabled']       			= 'Disabled';
$_['text_all_zones']       			= 'All Zones';
$_['text_off']           			= 'Off';
$_['text_bankart_slovenia']  		= '<a onclick="window.open(\'https://tebank.bankart.si/Testgw31/Merchant/userWelcome.jsp\');"><img src="view/image/payment/bankart_diners.png" alt="Bankart Slovenia" title="Bankart Slovenia" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_language']     			= 'Payment Module Language';
$_['entry_terminal_alias']     		= 'Terminal Alias';
$_['entry_terminal_password'] 		= 'Terminal Password:';
$_['entry_tranportal_id'] 			= 'TranPortal ID:';
$_['entry_currency'] 				= 'Currency:';
$_['entry_minimum_instalment'] 		= 'Minimum Instalment:';
$_['entry_order_status'] 			= 'Successful Order Status:';
$_['entry_declined_order_status'] 	= 'Declined Order Status:';
$_['entry_geo_zone']     			= 'Geo Zone:';
$_['entry_status']       			= 'Status:';
$_['entry_sort_order']   			= 'Sort Order:';

//Buttons
$_['button_save']       			= 'Save';
$_['button_cancel']   				= 'Cancel';

// Error
$_['error_terminal_alias']   		= 'Terminal Alias Required!';
$_['error_terminal_password']   	= 'Terminal Password Required!';
$_['error_tranportal_id']   		= 'TranPortal ID Required!';
?>