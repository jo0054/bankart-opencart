<?php 
class ModelPaymentBankartSlovenia extends Model {
  	public function getMethod($address) {
		$this->load->language('payment/bankart_slovenia');
		
		if ($this->config->get('bankart_slovenia_status')) {
      		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('bankart_slovenia_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
			
			if (!$this->config->get('bankart_slovenia_geo_zone_id')) {
        		$status = TRUE;
      		} elseif ($query->num_rows) {
      		  	$status = TRUE;
      		} else {
     	  		$status = FALSE;
			}	
      	} else {
			$status = FALSE;
		}
		
		$method_data = array();
	
		if ($status) {
      		$method_data = array( 
        		'code'       => 'bankart_slovenia',
        		'title'      => $this->language->get('text_title'),
				'sort_order' => $this->config->get('bankart_slovenia_sort_order')
      		);
    	}
		
		$query2 = $this->db->query("CREATE TABLE  IF NOT EXISTS " . DB_PREFIX . "bankart_slovenia (
		  `paymentID` VARCHAR(45) NOT NULL,
		  `currency` VARCHAR(10) NOT NULL,
		  `amount` DOUBLE NOT NULL,
		  `orderDetails` VARCHAR(150) NOT NULL,
		  `trackID` VARCHAR(45) NOT NULL,
		  `tranDate` VARCHAR(45) NOT NULL,
		  `name` VARCHAR(45) NOT NULL,
		  `addr1` VARCHAR(45) NOT NULL,
		  `addr2` VARCHAR(45) NOT NULL,
		  `addr3` VARCHAR(45) NOT NULL,
		  `city` VARCHAR(45) NOT NULL,
		  `state` VARCHAR(45) NOT NULL,
		  `postalCode` INTEGER UNSIGNED NOT NULL,
		  `result` VARCHAR(45) NOT NULL,
		  `auth` VARCHAR(45) NOT NULL,
		  `ref` VARCHAR(45) NOT NULL,
		  `tranID` VARCHAR(45) NOT NULL,
		  `postDate` VARCHAR(45) NOT NULL,
		  `udf1` VARCHAR(45) NOT NULL,
		  `udf2` VARCHAR(45) NOT NULL,
		  `udf3` VARCHAR(45) NOT NULL,
		  `udf4` VARCHAR(45) NOT NULL,
		  `udf5` VARCHAR(45) NOT NULL,
		  `responseCode` VARCHAR(45) NOT NULL,
		  `errMsg` VARCHAR(10) NOT NULL,
		  `errText` VARCHAR(150) NOT NULL,
		  `customerIP` VARCHAR(16) NOT NULL,
		  `eci` VARCHAR(2) NOT NULL,
		  PRIMARY KEY (`trackID`)
		)ENGINE = InnoDB;");
   
    	return $method_data;
  	}
  	
  	public function addOrder($order_details) {
  		$this->db->query("INSERT INTO " . DB_PREFIX . "bankart_slovenia (`paymentID`, `currency`, `amount`, `orderDetails`, `trackID`, `tranDate`, `name`, `addr1`, `addr2`, `addr3`, `city`, `state`, `postalCode`, `result`, `auth`, `ref`, `tranID`, `postDate`, `udf1`, `udf2`, `udf3`, `udf4`, `udf5`, `responseCode`, `errMsg`, `errText`, `customerIP`, `eci`) VALUES ('" . $this->db->escape($order_details['paymentID']) . "', '" . $this->db->escape($order_details['currency']) . "', '" . $this->db->escape($order_details['amount']) . "', '" . $this->db->escape($order_details['orderDetails']) . "', '" . $this->db->escape($order_details['trackID']) . "', '" . $this->db->escape($order_details['tranDate']) . "', '" . $this->db->escape($order_details['name']) . "', '" . $this->db->escape($order_details['addr1']) . "', '" . $this->db->escape($order_details['addr2']) . "', '" . $this->db->escape($order_details['addr3']) . "', '" . $this->db->escape($order_details['city']) . "', '" . $this->db->escape($order_details['state']) . "', '" . $this->db->escape($order_details['postalCode']) . "', '" . $this->db->escape($order_details['result']) . "', '" . $this->db->escape($order_details['auth']) . "', '" . $this->db->escape($order_details['ref']) . "', '" . $this->db->escape($order_details['tranID']) . "', '" . $this->db->escape($order_details['postDate']) . "', '" . $this->db->escape($order_details['udf1']) . "', '" . $this->db->escape($order_details['udf2']) . "', '" . $this->db->escape($order_details['udf3']) . "', '" . $this->db->escape($order_details['udf4']) . "', '" . $this->db->escape($order_details['udf5']) . "', '" . $this->db->escape($order_details['responseCode']) . "', '" . $this->db->escape($order_details['errMsg']) . "', '" . $this->db->escape($order_details['errText']) . "', '" . $this->db->escape($order_details['customerIP']) . "', '" . $this->db->escape($order_details['eci']) . "')");
  	}
  	
  	public function updateOrder($od) {
  		$this->db->query("UPDATE " . DB_PREFIX . "bankart_slovenia SET `result` ='" .   $this->db->escape($od['result']) . "', `auth` ='" . $this->db->escape($od['auth']) . "', `ref` ='" .   $this->db->escape($od['ref']) . "', `tranID` ='" .   $this->db->escape($od['tranID']) .  "', `postDate` ='" .   $this->db->escape($od['postDate']) .  "', `udf1` ='" .   $this->db->escape($od['udf1']) .  "', `errMsg` ='" .   $this->db->escape($od['errMsg']) .  "', `errText` ='" .   $this->db->escape($od['errText']) .  "', `eci` ='" .   $this->db->escape($od['eci']) . "' WHERE `paymentID` = '" . $od['paymentid'] . "'");
  	}
	
	public function getOrder($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "bankart_slovenia WHERE `trackID` LIKE '" .  (int)$order_id . "%'");
		return $query->row;
	}
}
?>