<?php
require_once DIR_APPLICATION . 'controller/payment/bankart_slovenia.e24PaymentPipe.php';

class ControllerPaymentBankartSlovenia extends Controller {
	
	protected function index() {
		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$this->language->load('payment/bankart_slovenia');
		
		//Calculate number of instalments available
		$amount = $this->currency->format($order_info['total'], $this->config->get('bankart_slovenia_currency_id'), '', false);
		$min_inst_amount = $this->config->get('bankart_slovenia_minimum_instalment');
		$max_instalments = floor($amount/$min_inst_amount);
		
		$this->data['max_instalments'] = $max_instalments;
		
		$this->data['text_select_inst'] = $this->language->get('text_select_inst');
		
    	$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['button_back'] = $this->language->get('button_back');
			
		$this->id = 'payment';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/bankart_slovenia.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/bankart_slovenia.tpl';
		} else {
			$this->template = 'default/template/payment/bankart_slovenia.tpl';
		}
		
		$this->render();
	}

	public function generateURL() {
		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($this->request->post['order_id']);
		
		$paymentPipe = new e24PaymentPipe();
		
		$paymentPipe->setResourcePath(DIR_ROOT);
		$paymentPipe->setAlias(trim($this->config->get('bankart_slovenia_terminal_alias')));
		$paymentPipe->setAction('4');
	    $paymentPipe->setAmt($this->currency->format($order_info['total'], $this->config->get('bankart_slovenia_currency_id'), '', false)); 
        $paymentPipe->setCurrency(978);
        $paymentPipe->setLanguage($this->config->get('bankart_slovenia_language'));
        $paymentPipe->setResponseURL(HTTPS_SERVER . 'bankart_callback.php');
        $paymentPipe->setErrorURL(HTTPS_SERVER . 'bankart_error.php');
		
		$orderDetails = '';
		
		$cartProducts = $this->cart->getProducts();
		
		foreach ($cartProducts as $cartProduct) {
			$orderDetails .= $cartProduct['quantity'] . "x " . $cartProduct['name'] . "\n";
				
				if(!empty($cartProduct['option'])) {
					foreach($cartProduct['option'] as $cartOptions) {
						$orderDetails.= " - " . $cartOptions['name'] . ': ' . $cartOptions['option_value'] . "\n";
					}
				}
		}
		
        $paymentPipe->setUdf1($this->request->post['instalments']);
		//$paymentPipe->setUdf2(urlencode($orderDetails));
		//$paymentPipe->setUdf3('Order ID: ' . $this->session->data['order_id']);
        $paymentPipe->setTrackId($this->request->post['order_id'] . "-" . md5(uniqid()));

        if ($paymentPipe->performPaymentInitialization() != $paymentPipe->SUCCESS) {
            $this->log->write("Bankart Slovenia Error: " . print_r($paymentPipe->getErrorMsg(), true));
			echo "<script type='text/javascript'>alert('" . print_r($paymentPipe->getErrorMsg(), true) . "');</script>";
			
			$this->redirect($this->url->link('checkout/checkout', '' ,'SSL'));
        } else {
        	//Save order in database
            $order_details = array(
				'paymentID' => $paymentPipe->getPaymentID(),
				'currency' => $paymentPipe->getCurrency(),
				'amount' => $paymentPipe->getAmt(),
				'orderDetails' => $orderDetails,
				'trackID' => $paymentPipe->getTrackID(),
				'tranDate' => date('d.m.Y H:i:s'),
				'name' => $order_info['payment_firstname'] . " " . $order_info['payment_lastname'],
				'addr1' => $order_info['payment_address_1'],
				'addr2' => $order_info['payment_address_2'],
				'addr3' => '',
				'city' => $order_info['payment_city'],
				'state' => $order_info['payment_zone'],
				'postalCode' =>  $order_info['payment_postcode'],
				'result' =>  '',
				'auth' =>  '',
				'ref' =>  '',
				'tranID' =>  '',
				'postDate' => '',
				'udf1' =>  $paymentPipe->getUdf1(),
				'udf2' =>  $paymentPipe->getUdf2(),
				'udf3' =>  $paymentPipe->getUdf3(),
				'udf4' =>  $paymentPipe->getUdf4(),
				'udf5' =>  $paymentPipe->getUdf5(),
				'responseCode' =>  '',
				'errMsg' =>  '',
				'errText' => '',
				'customerIP' => $this->getRealIpAddr(),
				'eci' =>  $order_info['payment_postcode']
			);
			
            $this->load->model('payment/bankart_slovenia');
			$this->model_payment_bankart_slovenia->addOrder($order_details);
			
			//$this->data['payment_url'] = str_replace("&amp;", "&", $paymentPipe->getPaymentPage()) . "PaymentID=" . $paymentPipe->getPaymentId();
	
			$data['paymenturl'] = str_replace("&amp;", "&", $paymentPipe->getPaymentPage()) . "PaymentID=" . $paymentPipe->getPaymentId();
			$this->redirect($data['paymenturl']);
		}
	}

	protected function getRealIpAddr() {
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	      $ip=$_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	    } else {
	      $ip=$_SERVER['REMOTE_ADDR'];
	    }
	    
	    return $ip;
	} 
	
	public function complete() {
		$this->load->model('payment/bankart_slovenia');
		
		$order_details = $this->model_payment_bankart_slovenia->getOrder($this->session->data['order_id']);
		
		if($order_details['result'] == "APPROVED") {
			$this->redirect($this->url->link('checkout/success', '' , 'SSL'));
		} else {
			if(!empty($order_details['errMsg'])) {
				$this->redirect($this->url->link('payment/bankart_slovenia/error', 'errText=' . $order_details['errText'], 'SSL'));
			} else {
				$this->redirect($this->url->link('payment/bankart_slovenia/error', '' , 'SSL'));
			}
		}
	}
	
	public function callback() {
		//if cURL call confirm order else redirect to success or error and checkout again page
		if(!empty($_POST)) {
			header("HTTP/1.1 200 OK");
			
			$this->load->model('checkout/order');
			
			$order_id = explode('-', $_POST['trackid']);
			$order_info = $this->model_checkout_order->getOrder($order_id[0]);
			
			//Check if approved if so confirm/update and redirect to success else send to error message page with error					   
			if($_POST['result'] == 'APPROVED') {
				$comment = "Payment ID: " . $_POST['result'] . "\n" .
					   	   "Response Code: " . $_POST['responsecode'] . "\n" .
					   	   "CVV2 Response: " . $_POST['cvv2response'];
				
				if(!$order_info['order_status_id']) {
					$this->model_checkout_order->confirm($order_id[0], $this->config->get('bankart_slovenia_order_status_id'), $comment, true);
				} else {
					$this->model_checkout_order->update($order_id[0], $this->config->get('bankart_slovenia_order_status_id'), $comment, true);
				}
				
				$this->load->model('payment/bankart_slovenia');
				
				if (isset($_POST['Error'])) {
					$errMsg = $_POST['Error'];
					$errText = 	 $_POST['ErrorText'];
				} else {
					$errMsg = '';
					$errText = 	 '';
				}
				
				$order_details = array(
					'paymentid' => $_POST['paymentid'],
					'result' =>  $_POST['result'],
					'auth' =>  $_POST['auth'],
					'ref' =>  $_POST['ref'],
					'tranID' =>  $_POST['tranid'],
					'postDate' =>  $_POST['postdate'],
					'trackid' =>  $_POST['trackid'],
					'udf1' =>  $_POST['udf1'],
					'responsecode' =>  $_POST['responsecode'],
					'cvv2response' =>  $_POST['cvv2response'],
					'eci' =>  $_POST['eci'],
					'errMsg' =>  $errMsg,
					'errText' =>  $errText
				);
				
				$this->model_payment_bankart_slovenia->updateOrder($order_details);
				
				echo "APPROVED";
			} else {
				/*$comment = "Result: " . $_POST['result'] . "\n" .
					   	   "Response Code: " . $_POST['responsecode'] . "\n";
				
				if(!$order_info['order_status_id']) {
					$this->model_checkout_order->confirm($order_id[0], $this->config->get('bankart_slovenia_declined_order_status_id'), $comment, false);
				} else {
					$this->model_checkout_order->update($order_id[0], $this->config->get('bankart_slovenia_declined_order_status_id'), $comment, false);
				}
				
				$this->load->model('payment/bankart_slovenia');
				
				$order_details = array(
					'paymentid' => $_POST['paymentid'],
					'result' =>  $_POST['result'],
					'auth' =>  $_POST['auth'],
					'ref' =>  $_POST['ref'],
					'tranid' =>  $_POST['tranid'],
					'postdate' =>  $_POST['postdate'],
					'trackid' =>  $_POST['trackid'],
					'udf1' =>  $_POST['udf1'],
					'responsecode' =>  $_POST['responsecode'],
					'cvv2response' =>  $_POST['cvv2response'],
					'eci' =>  $_POST['eci'],
					'errMsg' =>  $errMsg,
					'errText' =>  $errText
				);*/
				
				$this->model_payment_bankart_slovenia->updateOrder($order_details);
				
				echo "DECLINED";
			}
			
		} else {
			$this->log->write("Bankart Slovenia Callback Error");
		}
	}

	public function error() {	
		$this->language->load('payment/bankart_slovenia');
		
		$this->data['breadcrumbs'] = array(); 

      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('common/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => false
      	); 
		
      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('checkout/cart'),
        	'text'      => $this->language->get('text_basket'),
        	'separator' => $this->language->get('text_separator')
      	);
				
		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
			'text'      => $this->language->get('text_checkout'),
			'separator' => $this->language->get('text_separator')
		);	
					
      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('payment/bankart_slovenia/error'),
        	'text'      => $this->language->get('text_error'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		$this->data['heading_title'] = $this->language->get('heading_error');
		$this->document->setTitle($this->language->get('heading_error'));
		
		if(isset($this->request->get['errText'])) {
			$this->data['text_error_message'] = $this->language->get('text_error_message') . "<br /><p>" . $this->request->get['errText'] . "</p>";
		} else {
			$this->data['text_error_message'] = $this->language->get('text_error_message');
		}
		
		
		$this->data['button_continue'] = $this->language->get('button_continue');
		
    	$this->data['continue'] = $this->url->link('checkout/checkout');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/bankart_slovenia_error.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/bankart_slovenia_error.tpl';
		} else {
			$this->template = 'default/template/payment/bankart_slovenia_error.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'			
		);
				
		$this->response->setOutput($this->render());
	}
}
?>