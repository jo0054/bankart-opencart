<?php
function write($message) {
	$file = '/var/www/vhosts/bimbo.si/httpdocs/system/logs/error.txt';
	
	$handle = fopen($file, 'a+');
	
	fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message . "\n");
	
	fclose($handle); 
}

if(!empty($_POST)) {
	header('HTTP/1.0 200 OK');
		
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://www.bimbo.si/index.php?route=payment/bankart_slovenia/callback');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$result = curl_exec($ch);
	
	if(!$result) {
		write("cURL failed for Bankart Slovenia callback: " . print_r($_POST, true));
	} elseif ($result == "APPROVED") {
		echo "REDIRECT=https://www.bimbo.si/index.php?route=payment/bankart_slovenia/complete";
	} elseif($result == "DECLINED") {
		echo "REDIRECT=https://www.bimbo.si/index.php?route=payment/bankart_slovenia/complete";
	}
	
	curl_close($ch);
} else {
	header('Location: https://www.bimbo.si/index.php?route=payment/bankart_slovenia/complete');
}
?>