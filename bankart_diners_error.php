<?php
function write($message) {
	$file = '/var/www/vhosts/bimbo.si/httpdocs/system/logs/error.txt';
	
	$handle = fopen($file, 'a+');
	
	fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message . "\n");
	
	fclose($handle); 
}

if(!empty($_POST)) {
	header('HTTP/1.0 200 OK');	
	
	write("Error Callcback Info: " . print_r($_POST, true));
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://www.bimbo.si/index.php?route=payment/bankart_diners/callback');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$result = curl_exec($ch);
	curl_close($ch);
	
	if(!$result) {
		write("cURL failed for Bankart Slovenia error callback: " . print_r($_POST, true));
	} elseif ($result == "APPROVED") {
		echo "REDIRECT=https://www.bimbo.si/index.php?route=payment/bankart_diners/complete";
	} elseif($result == "DECLINED") {
		echo "REDIRECT=https://www.bimbo.si/index.php?route=payment/bankart_diners/complete";
	}
} else {
	header('Location: https://www.bimbo.si/index.php?route=payment/bankart_diners/complete');
}
?>