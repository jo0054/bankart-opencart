<?php
$_['heading_title'] = 'Zahvaljujemo se vam za nakup.';
$_['heading_error'] = 'POZOR - neuspešno plačilo!';
$_['text_title'] = '<b>NLB plačilne kartice</b><br>
Vse do 12 obrokov';
$_['text_error'] = 'Napaka';
$_['text_error_message'] = '<p>
	<strong><span style="font-size:14px;"><span style="font-family:arial;">Žal je bilo va&scaron;e plačilo neuspe&scaron;no.<img alt="" src="http://www.bimbo.si/image/data/slike-trgovine/pozor.png" style="width: 84px; height: 120px; float: right;" /></span></span></strong></p>
<p>
	<span style="font-size:14px;"><span style="font-family:arial;">Možni razlogi za neuspe&scaron;no plačilo so lahko:</span></span></p>
<ul>
	<li>
		<span style="font-family: arial; font-size: 14px;">pri vnosu podatkov o kartici ste se zatipkali,</span></li>
	<li>
		<span style="font-family: arial; font-size: 14px;">izbrali ste nakup na obroke (izberite &scaron;tevilko 1 za maestro kartico in vse kartice, ki niso izdane s strani NLB d.d.),</span></li>
	<li>
		<span style="font-size:14px;"><span style="font-family:arial;">pri&scaron;lo je do napake pri obdelavi.</span></span></li>
</ul>
<p>
	<strong><span style="font-size:14px;"><span style="font-family:arial;">Če menite, da je do napake pri&scaron;lo po pomoti vas prosimo, da poskusite znova.</span></span></strong></p>
';
$_['text_basket'] = 'Košarica';
$_['text_checkout'] = 'Na blagajni';
$_['text_select_inst'] = '<h1>Plačilo s plačilnimi karticami</h1>

<p><img alt="" src="https://www.bimbo.si/image/data/slike-trgovine/visa.jpg" style="width: 80px; height: 50px; margin-left: 10px; margin-right: 10px;"><img alt="" src="https://www.bimbo.si/image/data/slike-trgovine/Mastercard.gif" style="width: 80px; height: 50px; margin-left: 10px; margin-right: 10px;"><img alt="" src="https://www.bimbo.si/image/data/slike-trgovine/karanta.png" style="width: 97px; height: 50px; margin-left: 10px; margin-right: 10px;"><img alt="" src="https://www.bimbo.si/image/data/slike-trgovine/maestro.gif" style="width: 80px; height: 50px; margin-left: 10px; margin-right: 10px;"></p>

<p><span style="font-size:14px;"><span style="font-family:arial;">Naročeno blago lahko plačate z zgoraj navedenimi plačilnimi karticami vseh izdajateljic plačilnih kartic (<strong>vseh bank</strong>). Če imate plačilne kartice druge banke (ne NLB d.d.) pustite število obrokov na 1, četudi vam vaša banka omogoča plačilo z vašo kartico na obroke. Obročno plačevanje vam bo omogočila vaša banka skladno z navodili za obročna plačila za posamezne kartice. Števila 1 v tem primeru pomeni standardno transakcijo in ne vpliva na vaše število obrokov.</span></span></p>

<p><span style="font-size:14px;"><span style="font-family:arial;">Če pa ste lastnik plačilne kartice, ki jo je izdala <strong>NLB d.d.</strong> pa lahko naročeno blago plačate tudi na <strong>obroke</strong>, katere lahko izberete spodaj.&nbsp;</span></span></p>

<p><strong><span style="font-family: arial; font-size: 14px;">Gl</span><span style="font-family: arial; font-size: 14px;">avne prednosti plačila na obroke NLB d.d.</span></strong></p>

<p><img alt="" src="https://www.bimbo.si/image/data/slike-trgovine/3d-secure-verified-visa.png" style="font-family:arial; font-size:14px; width:148px; height:148px; margin-left:10px; margin-right:10px; float:right"></p>

<ul>
	<li><span style="font-size:14px;"><span style="font-family:arial;">enostavno plačilo na obroke z uporabo vaše NLB Plačilne kartice MasterCard, Visa in/ali Karanta (ne velja za posojilni kartici in kartico maestro)</span></span></li>
	<li><span style="font-size:14px;"><span style="font-family:arial;">brez dodatne dokumentacije</span></span></li>
	<li><span style="font-size:14px;"><span style="font-family:arial;">brez obresti in dodatnih stroškov.</span></span></li>
</ul>

<p><span style="font-family: arial; font-size: 14px;"><strong>Največje število obrokov je 12.</strong> <strong>Minimalni znesek posameznega obroka je 40 €.</strong> Spodaj lahko izberete med ponujenimi števili obrokov. Število ponujenih obrokov je odvisno od višine nakupa.&nbsp;</span><span style="font-family: arial; font-size: 14px;">Če želite plačati v enkratnem znesku izberite številko 1. Če je znesek nižji od 40€ ni možno izbrati števila obrokov.</span></p>

<p><span style="font-family: arial; font-size: 14px;"><strong>Po potrditvi nakupa boste preusmerjeni na 3d Secure procesni center BanKart, kjer boste vnesli podatke o vaši kartici</strong>. T</span><span style="font-family: arial; font-size: 14px;">ako mi kot trgovec&nbsp;</span><strong><span style="font-family: arial; font-size: 14px;">nikoli </span></strong><span style="font-family: arial; font-size: 14px;"><strong>nimamo vpogleda</strong> o p</span><span style="font-family: arial; font-size: 14px;">odatkih na vaši kartici.&nbsp;</span></p>

<p><img alt="" src="https://www.bimbo.si/image/data/slike-trgovine/nlb-na-obroke.png" style="width: 105px; height: 29px;"><br>
<span style="color:#FF0000;"><strong><span style="font-family: arial; font-size: 14px;">Izberite število obrokov:</span></strong></span>';
$_['button_continue'] = 'Nadaljujte';
?>