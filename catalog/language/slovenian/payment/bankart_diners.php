<?php
$_['heading_title'] = 'Zahvaljujemo se vam za nakup.';
$_['heading_error'] = 'POZOR - neuspešno plačilo!';
$_['text_title'] = '<b>Diners Club</b><br>
Vse do 24 obrokov';
$_['text_error'] = 'Napaka';
$_['text_error_message'] = '<p><strong><span style="font-size:14px;"><span style="font-family:arial;">Žal je bilo va&scaron;e plačilo neuspe&scaron;no.<img alt="" src="http://www.bimbo.si/image/data/slike-trgovine/pozor.png" style="width:84px; height:120px; float:right" /></span></span></strong></p>

<p><span style="font-size:14px;"><span style="font-family:arial;">Možni razlogi za neuspe&scaron;no plačilo so lahko:</span></span></p>

<ul>
	<li><span style="font-family: arial; font-size: 14px;">pri vnosu podatkov o kartici ste se zatipkali,</span></li>
	<li><span style="font-family: arial; font-size: 14px; line-height: 1.6;">pri&scaron;lo je do napake pri obdelavi.</span></li>
</ul>

<p><strong><span style="font-size:14px;"><span style="font-family:arial;">Če menite, da je do napake pri&scaron;lo po pomoti vas prosimo, da poskusite znova.</span></span></strong></p>
';
$_['text_basket'] = 'Košarica';
$_['text_checkout'] = 'Na blagajni';
$_['text_select_inst'] = '<h1><img alt="" height="66" src="https://www.bimbo.si/image/data/slike-trgovine/Diners-erste.png" width="350"></h1>

<h1><img alt="" height="150" src="https://www.bimbo.si/image/data/slike-trgovine/Diners-erste-1.png" style="float:right" width="150">Plačilo s plačilno kartico Diners Club</h1>

<p><span style="font-family: arial; font-size: 14px; line-height: 1.6;">Naročeno blago lahko plačate z zgoraj navedeno plačilno kartico Diners Club tudi v 24 mesečnih obrokih, če imate to možnost omogočeno s strani Dinersa.</span></p>

<p><span style="font-size:14px;"><span style="font-family:arial;">Nakupi na obroke vam v teh turbulentnih časih omogočajo, da si lahko privoščite vse tisto, kar si želite in potrebujete. Vaša kartica Diners Club vam ponuja možnost nakupe na obroke in tako razbremenitev vaših finančnih obveznosti na manjše zneske.&nbsp;</span></span></p>

<p><span style="font-family: arial; font-size: 14px;"><strong>Največje število obrokov je 24.</strong> <strong>Minimalni znesek posameznega obroka je 40 €.</strong> Spodaj lahko izberete med </span><img alt="" height="148" src="https://www.bimbo.si/image/data/slike-trgovine/3d-secure-verified-visa.png" style="float:right" width="148"><span style="font-family: arial; font-size: 14px;">ponujenimi števili obrokov. Število ponujenih obrokov je odvisno od višine nakupa.&nbsp;Če želite plačati v enkratnem znesku izberite številko 1. Če je znesek nižji od 40€ ni možno izbrati števila obrokov.</span></p>

<p><span style="font-family: arial; font-size: 14px;"><strong>Po potrditvi nakupa boste preusmerjeni na 3d Secure procesni center BanKart, kjer boste vnesli podatke o vaši kartici</strong>. Tako mi kot trgovec&nbsp;</span><strong><span style="font-family: arial; font-size: 14px;">nikoli </span></strong><span style="font-family: arial; font-size: 14px;"><strong>nimamo vpogleda</strong> o p</span><span style="font-family: arial; font-size: 14px;">odatkih na vaši kartici.&nbsp;</span></p>

<p><img alt="" height="63" src="https://www.bimbo.si/image/data/slike-trgovine/Diners-erste-2.png" width="130"><br>
<span style="color:#FF0000;"><strong><span style="font-family: arial; font-size: 14px;">Izberite število obrokov:</span></strong></span>';
$_['button_continue'] = 'Nadaljujte';
?>