<?php
// Heading
$_['heading_title']     	= 'Thank you for shopping with %s .... ';
$_['heading_error']     	= 'Bankart Slovenia - Error';

// Text
$_['text_title']        	= 'Bankart Slovenia';
$_['text_error']        	= 'Error';
$_['text_error_message']    = 'Your payment has been declined please try again:<br/>';
$_['text_basket']    		= 'Basket';
$_['text_checkout']    		= 'Checkout';
$_['text_select_inst']    	= 'Select number of instalments: ';

//Button
$_['button_continue']    	= 'Continue';
?>